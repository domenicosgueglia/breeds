
import breedsSaga from "./breedsSaga"

export default function* rootSaga(){
    yield [
        breedsSaga()
    ]
}