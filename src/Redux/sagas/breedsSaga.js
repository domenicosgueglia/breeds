import { call, put, take } from 'redux-saga/effects';
import * as types from '../constants/actions'
import request from '../services/dogsApi'

function* getBreeds() {
    while (true) {
        try {

            yield take(types.GET_BREEDS)
            const { status, message } = yield call(request.getBreedsList)
            if (status === 'success') {
                let breedObject = [];
                let response;
                for (let breed of Object.entries(message)) {
                    if (breed[1] && breed[1].length === 0) {
                        response = yield call(request.getBreedImage, breed[0])
                        if (response.status === 'success')
                        //manipulating object to put breed as key and img as value
                            breedObject = { ...breedObject, [breed[0]]: response.message }
                    } else {
                        for (let subreed of breed[1]) {
                            let response = yield call(request.getConcatBreedImage, subreed, breed[0])
                            if (response.status === 'success')
                             //manipulating object to put breed as key and img as value for composed breeds for example 'french bulldog'
                                breedObject = { ...breedObject, [`${subreed} ${breed[0]}`]: response.message }
                        }

                    }

                }
                //sending an action with an object to update the reducer with breeds
                yield put({ type: types.UPDATE_BREEDS_REDUCER, payload: { breeds: breedObject } })
            }
        } catch (e) {

        }
    }
}

//changing image for a single breed
function* changeImage() {
    while (true) {
        try {
            const { key } = yield take(types.CHANGE_IMAGE)
            let splittedBreed = key.split(' ');
            let response;
            let breedObject = {};
            if (splittedBreed && splittedBreed.length === 1) {
                response = yield call(request.getBreedImage, splittedBreed)
                if (response.status === 'success')
                    breedObject = { [splittedBreed]: response.message }
            } else {
                let response = yield call(request.getConcatBreedImage, splittedBreed[0], splittedBreed[1])
                if (response.status === 'success')
                    breedObject = { ...breedObject, [`${splittedBreed[0]} ${splittedBreed[1]}`]: response.message }
               
            }
            yield put({ type: types.UPDATE_SINGLE_BREED, payload: { breed: breedObject } })
        } catch (e) {

        }
    }
}

export default function* breedsSaga() {
    yield [getBreeds(),changeImage()]
}