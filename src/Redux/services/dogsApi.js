import axios from 'axios'


let instance = axios.create({ baseURL: 'https://dog.ceo/api/' })
let request = {

    getBreedsList() {
        return instance.get('breeds/list/all').
            then(response => response.data)
            .catch(error => error.response.data
            );
    },

    getBreedImage(breed){
        return instance.get(`breed/${breed}/images/random`).
        then(response => response.data)
        .catch(error => error.response.data
        );
    },
    getConcatBreedImage(subreed,breed){
        return instance.get(`breed/${breed}/${subreed}/images/random`).
        then(response => response.data)
        .catch(error => error.response.data
        );
    }

}

export default request