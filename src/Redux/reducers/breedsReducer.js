import * as types from '../constants/actions';

export default function reducer(state = {}, action) {
    switch (action.type) {
        case types.UPDATE_BREEDS_REDUCER: {
            //creating breeds object
            return { ...state, breeds: action.payload.breeds }
        }
        case types.UPDATE_SINGLE_BREED: {
            //updating single breed image
            return { ...state, breeds: { ...state.breeds, ...action.payload.breed } }
        }
    }
    return state;
}