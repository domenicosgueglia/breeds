import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from '../src/store'
import { Router, Route, browserHistory } from 'react-router';
import Home from '../src/Components/Home'
const store = configureStore();
import { Provider } from 'react-redux'
import 'semantic-ui-css/semantic.min.css';
import '../node_modules/font-awesome/css/font-awesome.css';
ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" component={Home} />
        </Router>
    </Provider>,
    document.getElementById('webapp'));
