import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import logger from "redux-logger"
import { rootReducer } from './Redux/reducers';
import rootSaga from './Redux/sagas/rootSaga'

const sagaMiddleware = createSagaMiddleware();


const middleware = applyMiddleware(sagaMiddleware, logger());


export default function configureStore() {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(rootReducer, composeEnhancers(middleware));
  sagaMiddleware.run(rootSaga);
  return store;
}


