import React from 'react'
import { connect } from 'react-redux'
import * as types from '../Redux/constants/actions'
import { Card, Image, Modal, Header, Button } from 'semantic-ui-react'
import styles from './Home.scss'
import map from 'lodash/map'

function mapStateToProps(state) {
    return {
        breeds: state.breedsReducer.breeds
    }
}
class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    componentDidMount() {
        //fetching breeds before mounting the page it takes time because there is no pagination
        this.props.dispatch({ type: types.GET_BREEDS })
    }

    renderCards(val, key) {
        return <Card fluid link>
            <Image src={val} height={200} width={300} />
            <Card.Content>
                <Card.Header className={styles.alignText}>{key}</Card.Header>
            </Card.Content>
        </Card>
    }

    changeImage(key) {
        //dispatching CHANGE IMAGE action to change image of a single breed
        this.props.dispatch({ type: types.CHANGE_IMAGE, key: key })
    }

    renderList() {
        const { breeds } = this.props;
        let listItem = map(breeds, (val, key) => <Modal key={key} trigger={this.renderCards(val, key)} className={styles.modalPosition}>
            <Modal.Header>Select a Photo <i title='Change photo' onClick={() => this.changeImage(key)} className={`fa fa-refresh ${styles.modalIconPosition}`}></i></Modal.Header>
            <Modal.Content>
                <Image wrapped size='medium' src={val} />
            </Modal.Content>
        </Modal>)
        return listItem
    }




    render() {
        const { breeds } = this.props;
        //if there are not breeds render the spinner
        if (breeds) {
            return (
                <div>
                    <div className={styles.navbarStyle}>Dog breeds</div>
                    <Card.Group itemsPerRow={6} className={styles.cardsMargin} >
                        {this.renderList()}
                    </Card.Group>
                </div>
            )
        } else {

            return <div><i className={`fa fa-spinner fa-spin ${styles.spinnerIcon}`} ></i><span className={styles.spinnerText}>Getting dog breeds...</span></div>
        }

    }
}

export default connect(mapStateToProps)(Home)