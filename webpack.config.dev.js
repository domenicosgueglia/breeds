const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');



module.exports = {
    context: path.join(__dirname),
    debug: true,
    devtool: "eval-source-map",
    entry: [
        "babel-polyfill",
        'webpack-hot-middleware/client?reload=true',
        "./src/index.js"
    ],
    output: {
        path: path.join(__dirname),
        filename: 'bundle.js',
        publicPath: '/'
    },
    plugins: [
        new ExtractTextPlugin('./src/styles/[name].css'),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        })
    
    ],
    module: {
        preLoaders: [
            {
                test: /\.(js|jsx)$/,
                include: /src/,
                loader: 'eslint-loader'
            }
        ],
        loaders: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader'
            },
            {
                test: /(\.css)$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader')
            },
            {
                test: /\.s?css$/,
                include: [
                  path.resolve(__dirname, 'src', 'Components')
                ],
                loader: 'style-loader!css-loader?modules=true&localIdentName=[name]__[local]___[hash:base64:5]!sass-loader!postcss-loader'
            },
            {
                test: /\.(png|jpg|gif|jpeg)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=100000'
            },
            {
                test: /\.(eot|com|json|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
            },
            {
                test: /\.(mp4|ogg|zip)$/,
                loader: 'file-loader'
            },
            {
                test: /\.swf$/,
                loader: "file?name=[path][name].[ext]"
            }
        ]
    },
    eslint: {
        configFile: './.eslintrc'
    },
    postcss: function () {
        return [autoprefixer];
    },
    node: {
      dns: 'mock',
      net: 'mock'
    }
};
